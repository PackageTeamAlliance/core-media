@extends('backend.layouts.master')

@section('page-title')
Edit Media
@stop


@section('main-panel-title')
Edit Media
@stop

@section('content')
<div class="row">

  <div class="col-md-9">

    <h4 class="m-t-0"></h4>

    <p class="text-center"><img src="{{url('uploads') .'/'.  $media->path}}" width="100%"></p>

    <br>
    
    <h4 class="m-t-0">Meta data</h4>
    <hr>
    <form method="POST">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">

        <label for="file">
          Altnerate Text
        </label>

        <input type="text" value="{{{old('alt', $media->alt)}}}" class="form-control" name="alt">

      </div>

      <div class="form-group">

        <label for="file">
          Description
        </label>

        <input type="text" value="{{{old('description', $media->description)}}}" class="form-control" name="description">

      </div>

      <div class="form-group">

        <label for="file">
          Long Description (Optional)
        </label>

        <textarea class="form-control" name="longdesc">{{{old('longdesc', $media->longdesc)}}}</textarea>

      </div>


      <button class="btn btn-success btn-block" v-on="click: saveTags"><i class="fa fa-save"></i> Save</button>
    </form>
  </div>

  <div class="col-md-3">


    <div class="panel panel-inverse">

      <div class="panel-heading">

        <h4 class="panel-title">Preset sizing, Information</h4>

      </div>


      <div class="panel-body" id="tags">
        <h4 class="m-t-0">Tagges</h4>

      
        <span class="label label-success" v-for="tag in tags" style="margin-left:5px; margin-top:5px; padding:5px 7px; font-size:12px;">@{{tag}}  <i class="fa fa-remove" v-on:click="removeTag" data-tag="@{{tag}}" style="font-size:12px;"></i></span> 

        <hr>

        <input type="text" value="" v-model="tag" class="form-control" @key.enter="saveTags">
        <hr>
        <button class="btn btn-success btn-block" v-on:click="saveTags"><i class="fa fa-save"></i> Add Tag</button>
      </div>
    </div> 
  </div>


</div>
@stop

@section('scripts')

<script>  
  var config = {
    token : '{{csrf_token()}}',
     save_url   : "{{ route('media.add.tag', [ $media->id ] ) }}",
    remove_url : "{{ route('media.remove.tag', [ $media->id ] ) }}",
    get_url    : "{{ route('media.get.tag', [ $media->id ] ) }}"
  };
</script>
<script src="{{url('module/assets/media/edit.js')}}"></script>
@stop