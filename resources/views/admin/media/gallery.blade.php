@extends('admin.layouts.gallery')

@section('page-title')
All Media
@stop


@section('main-panel-title')
All Media
@stop

@section('content')
{{-- Main content --}}
<div id="options" class="m-b-10">
    <span class="gallery-option-set" id="filter" data-option-key="filter">

        <button class="btn btn-default btn-xs active" data-option-value="">Show ALl</button>

        @foreach($tags->get() as $tag)
          <button class="btn btn-default btn-xs active" data-option-value="{{{$tag->name}}}" v-on="click: updateCategory">{{{$tag->name}}}</button>
        @endforeach
      
    </span>
</div>
 
<div id="gallery" class="gallery">
 
    <galleryimage v-repeat="med:media"></galleryimage>
</div>

@stop



@section('breadcrumb')

<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Home</a></li>
    <li><a href="javascript:;">Form Stuff</a></li>
    <li class="active">Form Elements</li>
</ol>
@stop


@section('scripts')

<script type="x-template" id="image-template">
   <div class="image">
        <div class="image-inner">
            <a href="#" v-on="click: editImage" >
                <img src="{{config('core-media.public_path')}}@{{med.path}}" alt=""/>
                
            </a>
            <p class="image-caption">
              @{{med.name}}
            </p>

              <div class="image-info">

                <div class="desc" style="padding-top:15px;">
                  <p class="text-center">
                    <a style="width:35%"; href="{{url('dashboard/media/edit')}}/@{{med.id}}"class="btn btn-success"><i class="fa fa-pencil"></i>  Edit</a>
                    <a style="width:35%"; href="{{url('dashboard/media/crop')}}/@{{med.id}}"class="btn btn-info"><i class="fa fa-crop"></i>  Crop</a>
                  </p>  
                  <p class="text-center">
                  </p>  
                </div>  
            </div>
        </div>
    </div>
</script>

<script>
  
  var config = {
    url : "{{route('media.ajax.all')}}"

  };

  Vue.component('galleryimage', {

    template: '#image-template',

    data: function(){
      return {
      
      }
    },
  });

  new Vue({
  
    el: "#gallery",
     data: {
        media: []
      },

      methods : {
          updateCategory : function(e)
          {
            alert(e.target);
          }
      },
      created: function() {
            // GET request
            this.$http.get(config.url, function (data, status, request) {

              // set data on vm
              this.$set('media', data);

              $("#gallery").isotope({
                resizable: true,
                masonry: {
                  columnWidth: 300
                }
              });

            }).error(function (data, status, request) {
              // handle error
          })
      }

  });


</script>
@stop