@extends('backend.layouts.master')

@section('page-header')
All Media
@stop

@section ('breadcrumbs')
<li><a href="{!!route('admin.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
<li class="active">{!! link_to_route('media.index', 'All Media') !!}</li>
@stop

@section('main-panel-title')
All Media
@stop

@section('content')
<style>
  table tr, table tbody tr td{
    word-wrap: break-word;
    max-width: 250px;
  }
</style>
<div class="box">
  <div class="box-body">
    <div class="row">
      <div class="col-md-6 col-md-offset-3" id="alert">
        <div class="alert alert-success text-center" role="alert">

        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>{{{ trans('pta/core-media::model.general.id') }}}</th>
            <th>{{{ trans('pta/core-media::model.general.name') }}}</th>
            <th>{{{ trans('pta/core-media::model.general.url') }}}</th>
            <th>{{{ trans('pta/core-media::model.general.type') }}}</th>
            <th>Visibility</th>
            <th>{{{ trans('pta/core-media::model.general.edit') }}}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($images as $image)
          <tr>
            <td>{{{$image->id}}}</td>
            <td style="width:200px;">
            <ul class="list-group">
            @if($image->crops->count())
              <li class="list-group-item" style="overflow-x:hidden; background-position: center; width:100%; display:block; height:250px; background-image : url('{{$image->crops()->where('type', 'Medium')->first()->path}}'); background-size:cover;"></li>
              @else
                <li class="list-group-item"><i class="fa fa-file" style="font-size:200px;"></i></li>
              @endif
            </ul>
      
            </td>
            <td>
             <label for="img-{{$image->id}}-original">Original</label>
             <div class="input-group" style="padding-bottom:5px;">
              <input type="text" class="form-control" id="img-{{$image->id}}-original" value="{{{$image->path}}}">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy" type="button" data-clipboard-target="#img-{{$image->id}}-original" data-toggle="tooltip" data-placement="top" title="copied" data-trigger="manual"><i class="fa fa-clipboard"></i></button>
              </span>
            </div>

            @foreach($image->crops as $crop)
            <label for="img-{{$image->id}}-{{$crop->type}}" style="padding-top:5px;">{{{$crop->type}}}</label>
            <div class="input-group">
              <input type="text" class="form-control" id="img-{{$image->id}}-{{$crop->type}}" value="{{$crop->path}}">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy" type="button" data-clipboard-target="#img-{{$image->id}}-{{$crop->type}}" data-toggle="tooltip" data-placement="top" title="copied" data-trigger="manual"><i class="fa fa-clipboard"></i></button>
              </span>
            </div>
            @endforeach
          </td>

          <td><span class="label label-primary">{{{$image->mime_type}}}</span></td>
          <td>
            @if($image->private)
            <span class="label label-danger">Private</span>
            @else
            <span class="label label-success">Public</span>
            @endif
            <td>
              <a class="btn btn-sm btn-info" href="{{{route('media.edit', [$image->id])}}}" target="_blank"> <i class="fa fa-pencil"></i> {{{ trans('pta/core-media::model.general.edit') }}}</a>
            </td>
            <td>
              <a class="btn btn-sm btn-primary" href="{{{$image->path}}}" target="_blank"> <i class="fa fa-eye"></i> {{{ trans('pta/core-media::model.general.view') }}}</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop


@section('sidebar')
<div class="panel panel-inverse">

  <div class="panel-heading">

    <h4 class="panel-title">{{ trans('pta/core-media::general.actions.upload')}}</h4>

  </div>

  <div class="panel-body">

    <a href="{{route('media.upload')}}" class="btn btn-primary ">{{{ trans('pta/core-media::general.actions.upload') }}}</a>

  </div>
</div>  
@stop

@section('scripts')
<script src="{{url('module/assets/media/index.js')}}"></script>
@stop