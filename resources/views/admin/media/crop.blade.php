@extends('backend.layouts.master')

@section('page-header')
<h1>Crop Media</h1>
@stop

@section ('breadcrumbs')
<li><a href="{!!route('admin.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
<li>{!! link_to_route('media.index', 'All Media') !!}</li>
<li class="active">Crop Media</li>
@stop

@section('content')

<!-- <h3 class="page-header">Demo:</h3> -->
<div class="img-container">
  <img src="{{url('images/'. $media->path)}}" alt="Picture">
</div>


<div>
  <h3 class="page-header">Preview:</h3>
  <div class="docs-preview clearfix">
    <div class="img-preview preview-lg"></div>
    <div class="img-preview preview-md"></div>
    <div class="img-preview preview-sm"></div>
    <div class="img-preview preview-xs"></div>
  </div>

  <!-- <h3 class="page-header">Data:</h3> -->
  <div class="docs-data">
         {{--  <div class="input-group">
            <label class="input-group-addon" for="dataX">X</label>
            <input class="form-control" id="dataX" type="text" placeholder="x">
            <span class="input-group-addon">px</span>
          </div>
          <div class="input-group">
            <label class="input-group-addon" for="dataY">Y</label>
            <input class="form-control" id="dataY" type="text" placeholder="y">
            <span class="input-group-addon">px</span>
          </div> --}}



          <div class="row">
            <!-- <h3 class="page-header">Toolbar:</h3> -->
            <div class="btn-group">
              <button class="btn btn-primary" data-method="setDragMode" data-option="move" type="button" title="Move">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;move&quot;)">
                  <span class="fa fa-arrows"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="setDragMode" data-option="crop" type="button" title="Crop">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;crop&quot;)">
                  <span class="fa fa-crop"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="zoom" data-option="0.1" type="button" title="Zoom In">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, 0.1)">
                  <span class="fa fa-search-plus"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="zoom" data-option="-0.1" type="button" title="Zoom Out">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, -0.1)">
                  <span class="fa fa-search-minus"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button" title="Rotate Left">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, -45)">
                  <span class="fa fa-undo"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="rotate" data-option="45" type="button" title="Rotate Right">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, 45)">
                  <span class="fa fa-repeat"></span>
                </span>
              </button>
            </div>

            <div class="btn-group">
              <button class="btn btn-primary" data-method="crop" type="button" title="Crop">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;crop&quot;)">
                  <span class="fa fa-check"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="clear" type="button" title="Clear">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;clear&quot;)">
                  <span class="fa fa-remove"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="disable" type="button" title="Disable">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;disable&quot;)">
                  <span class="fa fa-lock"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="enable" type="button" title="Enable">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;enable&quot;)">
                  <span class="fa fa-unlock"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="reset" type="button" title="Reset">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;reset&quot;)">
                  <span class="fa fa-refresh"></span>
                </span>
              </button>
              <button class="btn btn-primary" data-method="getCroppedCanvas" type="button">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getCroppedCanvas&quot;)">
                  Get Cropped Canvas
                </span>
              </button>
            </div>

          </div><!-- /.docs-buttons -->

        </div>
      </div>
      @stop


      @section('sidebar')
      <div class="panel panel-inverse">

        <div class="panel-heading">

          <h4 class="panel-title">Preset sizing, Information</h4>

        </div>

        <div class="panel-body">
          <h4 class="m-t-0">Toggles</h4>
          <div class="row">
            <div class="btn-group btn-group-justified" data-toggle="buttons">
              <label class="btn btn-primary active" data-method="setAspectRatio" data-option="1.7777777777777777" title="Set Aspect Ratio">
                <input class="sr-only" id="aspestRatio1" name="aspestRatio" value="1.7777777777777777" type="radio">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setAspectRatio&quot;, 16 / 9)">
                  16:9
                </span>
              </label>
              <label class="btn btn-primary" data-method="setAspectRatio" data-option="1.3333333333333333" title="Set Aspect Ratio">
                <input class="sr-only" id="aspestRatio2" name="aspestRatio" value="1.3333333333333333" type="radio">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setAspectRatio&quot;, 4 / 3)">
                  4:3
                </span>
              </label>
              <label class="btn btn-primary" data-method="setAspectRatio" data-option="1" title="Set Aspect Ratio">
                <input class="sr-only" id="aspestRatio3" name="aspestRatio" value="1" type="radio">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setAspectRatio&quot;, 1 / 1)">
                  1:1
                </span>
              </label>
              <label class="btn btn-primary" data-method="setAspectRatio" data-option="0.6666666666666666" title="Set Aspect Ratio">
                <input class="sr-only" id="aspestRatio4" name="aspestRatio" value="0.6666666666666666" type="radio">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setAspectRatio&quot;, 2 / 3)">
                  2:3
                </span>
              </label>
              <label class="btn btn-primary" data-method="setAspectRatio" data-option="NaN" title="Set Aspect Ratio">
                <input class="sr-only" id="aspestRatio5" name="aspestRatio" value="NaN" type="radio">
                <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setAspectRatio&quot;, NaN)">
                  Free
                </span>
              </label>
            </div>


          </div>

          <hr>

          <h4 class="m-t-0">Meta</h4>
          <div class="row">
            <div class="input-group">
              <label class="input-group-addon" for="dataWidth">Width</label>
              <input class="form-control" id="dataWidth" type="text" placeholder="width">
              <span class="input-group-addon">px</span>
            </div>
            <div class="input-group">
              <label class="input-group-addon" for="dataHeight">Height</label>
              <input class="form-control" id="dataHeight" type="text" placeholder="height">
              <span class="input-group-addon">px</span>
            </div>
            <div class="input-group">
              <label class="input-group-addon" for="dataRotate">Rotate</label>
              <input class="form-control" id="dataRotate" type="text" placeholder="rotate">
              <span class="input-group-addon">deg</span>
            </div>
          </div>

          <hr>



        </div>
      </div> 





      <form action="{{route('media.crop_save')}}">


      </form>

      @stop

      @section('scripts')

      <script>
        $(function () {

          'use strict';

          var console = window.console || { log: function () {} },
          $alert = $('.docs-alert'),
          $message = $alert.find('.message'),
          showMessage = function (message, type) {
            $message.text(message);

            if (type) {
              $message.addClass(type);
            }

            $alert.fadeIn();

            setTimeout(function () {
              $alert.fadeOut();
            }, 3000);
          };

  // Demo
  // -------------------------------------------------------------------------

  (function () {
    var $image = $('.img-container > img'),
    $dataX = $('#dataX'),
    $dataY = $('#dataY'),
    $dataHeight = $('#dataHeight'),
    $dataWidth = $('#dataWidth'),
    $dataRotate = $('#dataRotate'),
    options = {
          // data: {
          //   x: 420,
          //   y: 60,
          //   width: 640,
          //   height: 360
          // },
          // strict: false,
          // responsive: false,
          // checkImageOrigin: false

          // modal: false,
          // guides: false,
          // highlight: false,
          // background: false,

          // autoCrop: false,
          // autoCropArea: 0.5,
          // dragCrop: false,
          // movable: false,
          // rotatable: false,
          // zoomable: false,
          // touchDragZoom: false,
          // mouseWheelZoom: false,
          // cropBoxMovable: false,
          // cropBoxResizable: false,
          // doubleClickToggle: false,

          // minCanvasWidth: 320,
          // minCanvasHeight: 180,
          // minCropBoxWidth: 160,
          // minCropBoxHeight: 90,
          // minContainerWidth: 320,
          // minContainerHeight: 180,

          // build: null,
          // built: null,
          // dragstart: null,
          // dragmove: null,
          // dragend: null,
          // zoomin: null,
          // zoomout: null,

          aspectRatio: 16 / 9,
          preview: '.img-preview',
          crop: function (data) {
            $dataX.val(Math.round(data.x));
            $dataY.val(Math.round(data.y));
            $dataHeight.val(Math.round(data.height));
            $dataWidth.val(Math.round(data.width));
            $dataRotate.val(Math.round(data.rotate));
          }
        };

        $image.on({
      // 'build.cropper': function (e) {
      //   console.log(e.type);
      // },
      // 'built.cropper': function (e) {
      //   console.log(e.type);
      // },
      // 'dragstart.cropper': function (e) {
      //   console.log(e.type, e.dragType);
      // },
      // 'dragmove.cropper': function (e) {
      //   console.log(e.type, e.dragType);
      // },
      // 'dragend.cropper': function (e) {
      //   console.log(e.type, e.dragType);
      // },
      // 'zoomin.cropper': function (e) {
      //   console.log(e.type);
      // },
      // 'zoomout.cropper': function (e) {
      //   console.log(e.type);
      // },
      // 'change.cropper': function (e) {
      //   console.log(e.type);
      // }
    }).cropper(options);


    // Methods
    $(document.body).on('click', '[data-method]', function () {
      var data = $(this).data(),
      $target,
      result;

      if (!$image.data('cropper')) {
        return;
      }

      if (data.method) {
        data = $.extend({}, data); // Clone a new one

        if (typeof data.target !== 'undefined') {
          $target = $(data.target);

          if (typeof data.option === 'undefined') {
            try {
              data.option = JSON.parse($target.val());
            } catch (e) {
              console.log(e.message);
            }
          }
        }

        result = $image.cropper(data.method, data.option);

        if (data.method === 'getCroppedCanvas') {

          // var file = new FormData(this.$avatarForm[0]),
          var dataURL = result.toDataURL(),
          url = "{{route('media.crop_save')}}";
          $.ajaxSetup({
            headers: {
              'X-CSRF-Token': "{{csrf_token()}}"
            }
          });

          $.ajax(url, {
            type: 'POST',
            data: { 
              imgBase64: dataURL
            },
            dataType: 'json',
            
            // processData: false,
            // contentType: false,

            beforeSend: function () {
              // _this.submitStart();
            },

            success: function (data) {
              console.log(data);
              // _this.submitDone(data);
            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
              // _this.submitFail(textStatus || errorThrown);
            },

            complete: function () {
              // _this.submitEnd();
            }
          });
          // $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);
        }

        if ($.isPlainObject(result) && $target) {
          try {
            $target.val(JSON.stringify(result));
          } catch (e) {
            console.log(e.message);
          }
        }

      }
    }).on('keydown', function (e) {

      if (!$image.data('cropper')) {
        return;
      }

      switch (e.which) {
        case 37:
        e.preventDefault();
        $image.cropper('move', -1, 0);
        break;

        case 38:
        e.preventDefault();
        $image.cropper('move', 0, -1);
        break;

        case 39:
        e.preventDefault();
        $image.cropper('move', 1, 0);
        break;

        case 40:
        e.preventDefault();
        $image.cropper('move', 0, 1);
        break;
      }

    });


    // Import image
    var $inputImage = $('#inputImage'),
    URL = window.URL || window.webkitURL,
    blobURL;

    if (URL) {
      $inputImage.change(function () {
        var files = this.files,
        file;

        if (!$image.data('cropper')) {
          return;
        }

        if (files && files.length) {
          file = files[0];

          if (/^image\/\w+$/.test(file.type)) {
            blobURL = URL.createObjectURL(file);
            $image.one('built.cropper', function () {
              URL.revokeObjectURL(blobURL); // Revoke when load complete
            }).cropper('reset').cropper('replace', blobURL);
            $inputImage.val('');
          } else {
            showMessage('Please choose an image file.');
          }
        }
      });
    } else {
      $inputImage.parent().remove();
    }


    // Options
    $('.docs-options :checkbox').on('change', function () {
      var $this = $(this);

      if (!$image.data('cropper')) {
        return;
      }

      options[$this.val()] = $this.prop('checked');
      $image.cropper('destroy').cropper(options);
    });


    // Tooltips
    $('[data-toggle="tooltip"]').tooltip();

  }());

});


</script>
@stop