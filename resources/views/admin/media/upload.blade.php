@extends('backend.layouts.master')


@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/media/media.css')}}">
@stop
@section('page-header')
Media Uploads
@stop


@section('main-panel-title')
Upload Media
@stop

@section ('breadcrumbs')
<li><a href="{!!route('admin.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
<li>{!! link_to_route('media.index', 'All Media') !!}</li>
<li class="active">Upload Media</li>
@stop

@section('content')
<div class="box">
  <div class="box-body">
    <div id="actions" class="row">

      <div class="col-lg-7">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <span class="btn btn-success fileinput-button">
          <i class="fa fa-plus"></i>
          <span>Add files...</span>
        </span>
        <button type="submit" class="btn btn-primary start">
          <i class="fa fa-upload"></i>
          <span>Start upload</span>
        </button>
        <button type="reset" class="btn btn-warning cancel">
          <i class="fa fa-times"></i>
          <span>Cancel upload</span>
        </button>
      </div>

      <div class="col-lg-5">
        <!-- The global file processing state -->
        <span class="fileupload-process">
          <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
          </div>
        </span>
      </div>

    </div>




    <div class="table table-striped" class="files" id="previews">

      <div id="template" class="file-row">
        <!-- This is used as the file preview template -->
        <div>
          <span class="preview"><img data-dz-thumbnail /></span>
        </div>
        <div>
          <p class="name" data-dz-name></p>
          <strong class="error text-danger" data-dz-errormessage></strong>
        </div>
        <div>
          <p class="size" data-dz-size></p>
          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
          </div>
        </div>
        <div>
          <button class="btn btn-primary start">
            <i class="fa fa-upload"></i>
            <span>Start</span>
          </button>
          <button data-dz-remove class="btn btn-warning cancel">
            <i class="fa fa-times"></i>
            <span>Cancel</span>
          </button>
          <a class="btn btn-success view" target="_blank">
            <i class="fa fa-eye"></i>
            <span>View</span>
          </a>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('scripts')
<script>
  var app = {
    'upload_url' : "{{route('media.upload')}}",
    'token' : "{{csrf_token()}}"
  };
</script>
<script src="{{url('module/assets/media/upload.js')}}"></script>
@stop