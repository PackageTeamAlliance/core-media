@extends('backend.layouts.master')

@section ('title', 'Edit Media')

@section('page-header')
<h1>Edit Media</h1>
@stop

@section ('breadcrumbs')
<li><a href="{!!route('admin.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
<li>{!! link_to_route('media.index', 'All Media') !!}</li>
<li class="active">Edit Media</li>
@stop

@section('main-panel-title')
Edit Media
@stop

@section('content')
<div class="row">
  <div class="col-md-9"><img src="{{$media->path}}" height="400px">
  </div>

  <div class="col-md-3">
    
    
  </div>
</div>{{-- End top row --}}
<hr>
<div class="row">
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="panel-heading"> <h4>Meta data</h4></div>
        <form method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label for="file">Altnerate Text</label>
            <input type="text" value="{{{old('alt', $media->alt)}}}" class="form-control" name="alt">
          </div>
          <div class="form-group">
            <label for="file">Description</label>

            <input type="text" value="{{{old('description', $media->description)}}}" class="form-control" name="description">

          </div>

          <div class="form-group">

            <label for="file">
              Long Description (Optional)
            </label>

            <textarea class="form-control" name="longdesc">{{{old('longdesc', $media->longdesc)}}}</textarea>

          </div>


          <button class="btn btn-success btn-block" v-on="click: saveTags"><i class="fa fa-save"></i> Save</button>
        </form>
      </div>
    </div>
  </div>

  <div class="col-md-4"  id="tags">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="panel-heading"> <h4>Tags</h4></div>


        <span class="label label-success" v-for="tag in tags" style="margin-left:5px; margin-top:5px; padding:5px 7px; font-size:12px;">@{{tag}}  <i class="fa fa-remove" v-on:click="removeTag" data-tag="@{{tag}}" style="font-size:12px;"></i></span> 

        <hr>

        <input type="text" value="" v-model="tag" class="form-control" @keyup.enter="saveTags">
        <hr>
        <button class="btn btn-success btn-block" v-on:click="saveTags"><i class="fa fa-save"></i> Add Tag</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts')
<script>  
  var config = {
    token : '{{csrf_token()}}',
    save_url   : "{{ route('media.add.tag', [ $media->id ] ) }}",
    remove_url : "{{ route('media.remove.tag', [ $media->id ] ) }}",
    get_url    : "{{ route('media.get.tag', [ $media->id ] ) }}"
  };
</script>
<script src="{{url('module/assets/media/edit.js')}}"></script>
@stop