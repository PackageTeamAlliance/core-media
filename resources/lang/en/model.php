<?php

return [

	'general' => [
		'id'          => 'Id',
		'name'        => 'Name',
		'url'         => 'URL',
		'type'        => 'Type',
		'html'        => 'Html',
		'view'        => 'View Image',
		'edit'        => 'Edit Media',
		'file'        => 'File',
		'crop'        => 'Crop Media',
		'locale'      => 'Locale',
		'translation' => 'Translations',
		'created_at'  => 'Created At',
		'name_help'   => 'Enter the Name here',
		'slug_help'   => 'Enter the Slug here',
		'type_help'   => 'Enter the Type here',
		'html_help'   => 'Enter the Html here',
		'value_help'  => 'Enter the Value here',
		'file_help'   => 'Enter the File here',
		'locale_help' => 'Enter the Language translation',

	],

];
