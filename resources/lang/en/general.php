<?php 

return [
		

	'title' => [
	
		'index' => 'All Media',
		'meta'  => 'Media Meta'
	
	], 

	'header' => [
	
		'index' => 'Media',
	
	], 

	'actions' => [
	
		'add'    => 'Add Media',
		'edit'   => 'Edit Media',
		'delete' => 'Delete Media',
		'upload' => 'Upload Media',
	]

];