  Vue.http.headers.common['X-CSRF-TOKEN'] = config.token;
  
  new Vue({

    el: "#tags",
    data: {
      tags: [],
      tag:'',
      loading: "Message",
      button: ''
    },

    created: function() {
      this.$http.get(config.get_url, function (data, status, request) {

            // set data on vm
            this.tags = data;

          }).error(function (data, status, request) {
              // handle error
            });
        },
        methods: {

          saveTags :function(e)
          {

            if(this.tag == "")
            {
              alert('add a tag!');
              return false;
            }

            this.button = e.target;

            $(this.button).button('loading');

            var data = {tags: this.tag};

            this.$http.post(config.save_url, data, function(data, status, request){

              this.tags = data;

              $(this.button).button('reset');

              this.tag = "";

            }).error(function (data, status, request) {

              console.log(status);
            })

          }
          ,

          removeTag : function(e){

            var $tag = e.target.getAttribute('data-tag');

            var data = {tags: $tag};

            this.$http.post(config.remove_url, data, function(data, status, request){

              this.tags = data;
              this.tag = '';


            }).error(function (data, status, request) {

              console.log('error', status);
            })


          }
        }

      });