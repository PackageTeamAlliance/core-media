$('#alert').hide();
var clipboard = new Clipboard('.btn-copy');
clipboard.on('success', function(e) {
	$(e.trigger).tooltip('show');
	setTimeout(function(){
		$(e.trigger).tooltip('hide');
	},2000);
});