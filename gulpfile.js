var elixir = require('laravel-elixir');

elixir(function(mix) {

    mix
        // .browserify([
        //     'components/content-datatable.js',

        // ], './resources/assets/js/bundle/bundle.js')
        .scripts([
            '../../../node_modules/clipboard/dist/clipboard.js',
            'index.js',
        ], './resources/assets/modules/media/index.js')

        .scripts([
            '../../../node_modules/vue/dist/vue.js',
            '../../../node_modules/vue-resource/dist/vue-resource.js',
            'edit.js',
        ], './resources/assets/modules/media/edit.js')

        .scripts([
            '/../../../node_modules/dropzone/dist/dropzone.js',
            'media.js',

        ], './resources/assets/modules/media/upload.js')
        .styles([
            '../../../node_modules/dropzone/dist/dropzone.css',
            'media.css',
        ], './resources/assets/modules/media/media.css')
});
