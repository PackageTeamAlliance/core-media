<?php

return [
	
	'name' => 'Core Media',
	
    /*
     * Default prefix to the dashboard.
     */
    'route_prefix' => config('core.admin_uri') . 'media',

    /*
     * Default permission user should have to access the dashboard.
     */
       'security' => [
        'protected'          => true,
        'middleware'         => ['web', 'access.routeNeedsPermission:manage-media'],
        'permission_name'    => 'manage-media',
    ],


  'permissions' => [
        'view' => [
            'name' => 'manage-media',
            'display_name' => 'Manage Media'
        ],
        'create' => [
            'name' => 'create-media',
            'display_name' => 'Create Media'
        ],
        'edit' => [
            'name' => 'edit-media',
            'display_name' => 'Edit Media'
        ],
        'delete' => [
            'name' => 'delete-media',
            'display_name' => 'Delete Media'
        ]
    ],

    /*
     * Default url used to redirect user to front/admin of your the system.
     */
    'system_url' => config('core.redirect_url'),

    'public_path' => '/uploads/'

];
