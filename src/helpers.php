<?php
if (! function_exists('media_path')) {
    function media_path($file)
    {
        if (config('filesystems.default') === 's3') {
            $region = config('filesystems.disks.s3.region');
            $bucket = config('filesystems.disks.s3.bucket');
            return "https://s3-{$region}.amazonaws.com/{$bucket}/{$file}";
        }

        if (config('filesystems.default') === 'local') {
            return  url("uploads/{$file}");
        }
    }
}
