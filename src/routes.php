<?php

	/*
	|--------------------------------------------------------------------------
	| Admin routes for Media uploads
	|--------------------------------------------------------------------------
	|
	| 
	|
	*/

	$router->group(['namespace' => 'Pta\CoreMedia\Http\Controllers\Admin'], function () use ($router) {

			$router->get('/', ['as'=> 'media.index', 'uses' => 'MediaController@index']);
	
			$router->get('all', ['as'=> 'media.ajax.all', 'uses' => 'MediaController@all']);
			$router->get('gallery', ['as'=> 'media.gallery', 'uses' => 'MediaController@gallery']);
	
			$router->get('upload', ['as'=> 'media.upload', 'uses' => 'MediaController@upload']);
			$router->post('upload', ['as'=> 'media.upload.post', 'uses' => 'MediaController@upload_process']);
	
			$router->get('{id}/edit', ['as'=> 'media.edit', 'uses' => 'MediaController@edit']);
			$router->post('{id}/edit', ['as'=> 'media.edit.process', 'uses' => 'MediaController@edit_process']);
	
			$router->get( 'edit/{id}/tag', ['as'=> 'media.get.tag', 'uses' => 'MediaController@get_tags']);
			$router->post('edit/{id}/tag', ['as'=> 'media.add.tag', 'uses' => 'MediaController@add_tags']);
			$router->post('remove/{id}/tag', ['as'=> 'media.remove.tag', 'uses' => 'MediaController@remove_tags']);
	
			$router->get('{id}/crop', ['as'=> 'media.crop', 'uses' => 'MediaController@crop']);
			$router->post('crop/save', ['as'=> 'media.crop_save', 'uses' => 'MediaController@crop_save']);

	});    


$router->group(['namespace' => 'Pta\CoreMedia\Http\Controllers\Frontend'], function () use ($router) {
	// get('asset/image/{id}', ['uses' => 'MediaController@asset', 'as' => 'image.route']);
});