<?php

namespace Pta\CoreMedia\Providers;

use Illuminate\Support\Facades\Blade;
use Pta\CoreMedia\Widget\MediaWidget;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Pta\Media\Providers\PackageServiceProvider as MediaServiceProvider;

class PackageServiceProvider extends BaseServiceProvider
{
    protected $providers = [MediaServiceProvider::class ];
    

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerBindings();
        
        $this->registerConfig();
        
        $this->registerViews();

        $this->registerTranslations();

        $this->registerMigration();

        $this->registerRoutes();

        $this->registerAssets();


        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
      /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();

        $this->bindFacade();

        $this->registerBladeExtension();
    }

        /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings()
    {
    }

        /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $configPath = realpath(__DIR__ . '/../../config/config.php');
        
        $this->publishes([ $configPath => config_path('core-media.php'), 'config']);
        
        $this->mergeConfigFrom($configPath, 'core-media');
    }


        /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/core-media');
        
        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/core-media')]);
    }

    public function registerAssets()
    {
        $this->publishes([
          realpath(__DIR__ . '/../../resources/assets/modules') => public_path('module/assets/'),
        ], 'core-assets');
    }

    public function registerTranslations()
    {
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/core-media');
    }

    public function registerMigration()
    {
        $this->publishes([realpath(__DIR__ .'/../../config/config.php') => config_path('core-media.php') ], 'config');
    }

    public function registerRoutes()
    {
        $router = $this->app['router'];
        
        $prefix = config('core-media.route_prefix');
        


        $security = $this->app['config']->get('core-media.security.protected', true);

        if (!$this->app->routesAreCached()) {
            $group = [];
            
            $group['prefix'] = $prefix;
            
            if ($security) {
                $group['middleware'] = config('core-media.security.middleware');
                $group['can'] = config('core-media.security.permission_name');
            }
            
            $router->group($group, function () use ($router) {
                
                require realpath(__DIR__ . '/../routes.php');
            });
        }
    }


    protected function bindFacade()
    {
        $this->app['core.media'] = $this->app->share(function ($app) {
            return new MediaWidget($app);
        });
        
        $loader = AliasLoader::getInstance();
        
        $loader->alias('Image', '\Pta\CoreMedia\Facades\MediaFacade');
    }


    public function registerBladeExtension()
    {
        Blade::directive('image', function ($value) {
            return "<?php echo Image::show(array$value); ?>";
        });

        Blade::directive('download', function ($id) {
            return "<?php echo Image::download(array$id); ?>";
        });
    }
    // public function boot() {

    //     $this->publishes([realpath(__DIR__ . '/../../resources/migrations') => $this->app->databasePath() . '/migrations'], 'migrations');

    //     $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/core-media');

    //     if (!$this->app->routesAreCached()) {
    //         require realpath(__DIR__ . '/../routes.php');
    //     }

    //     $this->publishes([realpath(__DIR__ .'/../../config/config.php') => config_path('core-media.php') ], 'config');
    // }

    // /**
    //  * {@inheritDoc}
    //  */
    // public function register() {

    //     foreach ($this->providers as $provider) {
    //         $this->app->register($provider);
    //     }

    //     $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/core-media');

    //     $this->mergeConfigFrom( realpath(__DIR__ .'/../../config/config.php'), 'core-media');
    // }
}
