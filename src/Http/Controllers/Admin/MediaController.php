<?php

namespace Pta\CoreMedia\Http\Controllers\Admin;

use Uploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Pta\Media\Repositories\Media\MediaRepositoryInterface;

class MediaController extends BaseController
{
    /**
     * The Media repository.
     *
     * @var \Pta\Media\Repositories\Media\MediaRepositoryInterface
     */
    protected $media;
    
    /**
     * Constructor.
     *
     * @param  \Pta\Media\Repositories\Media\MediaRepositoryInterface  $media
     * @return void
     */
    public function __construct(MediaRepositoryInterface $media)
    {
        $this->media = $media;
    }
    
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $images = Uploader::findAll();
        
        return view('pta/core-media::admin.media.index', compact('images'));
    }

  
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function gallery()
    {
        $tags = $this->media->createModel()->allTags();

        return view('pta/core-media::admin.media.gallery', compact('tags', 'images'));
    }


    public function all()
    {
        return Uploader::findAll();
    }

    public function upload()
    {
        return view('pta/core-media::admin.media.upload');
    }

    public function upload_process(Request $request)
    {
        $insert = Uploader::save($request->file('file'));

        return $insert;
    }


    public function edit($id)
    {
        $media = $this->media->find($id);
        
        return view('pta/core-media::admin.media.edit')->with(compact('media'));
    }

    public function edit_process(Request $request, $id)
    {
        $media = $this->media->find($id);

        $this->media->store($id, $request->all());

        flash()->success('Successfully saved');
        
        return back();
    }

    public function get_tags($id)
    {
        $media = $this->media->find($id);

        return $media->tags->lists('name');
    }

    public function add_tags(Request $request, $id)
    {
        $media = $this->media->find($id);

        $tag = $request->get('tags');

        $media->tag($tag);

        $media = $this->media->find($id);

        return $media->tags->lists('name');
    }

    public function remove_tags(Request $request, $id)
    {
        $media = $this->media->find($id);

        $tag = $request->get('tags');

        $media->untag($tag);

        $media = $this->media->find($id);

        return $media->tags->lists('name');
    }

    public function crop($id)
    {
        $media = $this->media->find($id);
        
        return view('pta/core-media::admin.media.crop')->with(compact('media'));
    }

    public function crop_save()
    {
        $img = Input::get('imgBase64');

        $img = str_replace('data:image/png;base64,', '', $img);

        $img = str_replace(' ', '+', $img);

        $data = base64_decode($img);

        $insert = Uploader::saveFromCanvas($data,  uniqid() . '.png');

        return $insert;
    }
}
