<?php

namespace Pta\Media\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Pta\Media\Repositories\Crop\CropRepositoryInterface;

class CropsController extends BaseController
{
    use DispatchesJobs;
    
    /**
     * The Media repository.
     *
     * @var \Pta\Media\Repositories\Crop\CropRepositoryInterface
     */
    protected $crops;
    
    /**
     * Constructor.
     *
     * @param  \Pta\Media\Repositories\Crop\CropRepositoryInterface  $crops
     * @return void
     */
    public function __construct(CropRepositoryInterface $crops)
    {
        parent::__construct();
        
        $this->crops = $crops;
    }
    
    /**
     * Display a listing of crop.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pta/media::crops.index');
    }
    
    /**
     * Datasource for the crop Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid()
    {
        $data = $this->crops->grid();
        
        // $columns = [
        // 	'id',
        // 	'media_id',
        // 	'type',
        // 	'path',
        // 	'created_at',
        // ];

        // $settings = [
        // 	'sort'      => 'created_at',
        // 	'direction' => 'desc',
        // ];

        // $transformer = function($element)
        // {
        // 	$element->edit_uri = route('admin.pta.media.crops.edit', $element->id);

        // 	return $element;
        // };

        // return datagrid($data, $columns, $settings, $transformer);
    }
    
    /**
     * Show the form for creating new crop.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }
    
    /**
     * Handle posting of the form for creating new crop.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        return $this->processForm('create');
    }
    
    /**
     * Show the form for updating crop.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }
    
    /**
     * Handle posting of the form for updating crop.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }
    
    /**
     * Remove the specified crop.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $type = $this->crops->delete($id) ? 'success' : 'error';
        
        $this->alerts->{$type}(trans("/media::crops/message.{$type}.delete"));
        
        return redirect()->route('admin.pta.media.crops.all');
    }
    
    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction()
    {
        $action = request()->input('action');
        
        $this->crops->{$action}($row);
    }
    
    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        
        // Do we have a crop identifier?
        if (isset($id)) {
            if (!$crop = $this->crops->find($id)) {
                $this->alerts->error(trans('pta/media::crops/message.not_found', compact('id')));
                
                return redirect()->route('admin.pta.media.crops.all');
            }
        } else {
            $crop = $this->crops->createModel();
        }
        
        // Show the page
        return view('pta/media::crops.form', compact('mode', 'crop'));
    }
    
    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        
        // Store the crop
        list($messages) = $this->crops->store($id, request()->all());
        
        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("pta/media::crops/message.success.{$mode}"));
            
            return redirect()->route('admin.pta.media.crops.all');
        }
        
        $this->alerts->error($messages, 'form');
        
        return redirect()->back()->withInput();
    }
}
