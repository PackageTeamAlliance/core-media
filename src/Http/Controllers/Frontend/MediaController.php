<?php

namespace Pta\CoreMedia\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class MediaController extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('pta/core-media::media.index');
    }


    /**
     * Return the upload view.
     *
     * @return \Illuminate\View\View
     */
    public function upload() {
        return view('pta/core-media::media.upload');
    }
}
