<?php

namespace Pta\CoreMedia\Widget;

use Uploader;
use Illuminate\Container\Container;
use Intervention\Image\Facades\Image;

class MediaWidget
{
    /**
     * The App container.
     *
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * 
     *
     * @var \Filesystem
     */
    protected $manager;

    /**
     * 
     *
     * @var \Media repo
     */
    protected $media;
    
    public function __construct(Container $app)
    {
        $this->app = $app;

        $this->media = app('pta.media.media');

        $this->manager = $this->app['filesystem'];
    }
    
    public function show($data)
    {
        $id = $data[0];
        $media =  $this->media->find((int) $id);
        $size = null;

        if ($length = count($data) > 1) {
            $size = $data[1];
        }

        if ($this->manager->exists($media->name)) {
            if ($size == null) {
                return $media->path;
            } else {
                if($media->crops->count())
                {
                    return $this->media->getCropSize($id, $size);    
                }
            }
        }
        // dd(count($data) );
        // list($id, $size);

        // 

        // if ($this->manager->exists($media->name)) {
        //     // if ($media->crops->count() && $size == null) {

        //         return $media->path;
        //     // }else{

        //     // }

        // }
    }

    public function download($id)
    {
        $media =  $this->media->find((int) $id);

        $path = "{$media->id}/{$media->name}";

        if ($this->manager->exists($path)) {
            return response()->download($media->path);
        }
    }
}
