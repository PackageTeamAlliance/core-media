<?php

namespace Pta\CoreMedia\Console\Installers;

use Illuminate\Console\Command;
use Pta\Pulse\Console\Contracts\Installer;

class CoreMediaInstaller implements Installer
{
    
    /**
     * @var Command
     */
    protected $command;
    
    public function run(Command $command) {
     
     	$this->command = $command;

     	$this->command->call('module:migrate', ['module' => 'CoreMedia']);

        $this->command->call('module:seed', ['module' => 'CoreMedia']);
    }
}
