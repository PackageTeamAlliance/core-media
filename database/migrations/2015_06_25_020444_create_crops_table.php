<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCropsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_crops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('media_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('name')->index();
			$table->string('type')->index();
			$table->integer('private')->default('0')->unsigned();
			$table->string('path')->length(450);
			$table->integer('height')->default('0')->unsigned();
			$table->integer('width')->default('0')->unsigned();
			$table->integer('file_size')->default('0')->unsigned();
			$table->string('mime_type')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_crops');
	}

}
