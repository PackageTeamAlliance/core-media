<?php

// namespace Modules\CoreMedia\Database\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMediaTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('media', function (Blueprint $table) {
          
            $table->string('alt')->nullable();
            $table->string('description')->nullable();
            $table->text('longdesc')->nullable();
       
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

         Schema::table('media', function (Blueprint $table) {
          
            $table->dropColumn('alt');
            $table->dropColumn('description');
            $table->dropColumn('longdesc');
       
        });
    }
}
