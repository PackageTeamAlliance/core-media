<?php

namespace Modules\CoreMedia\Database\Seeders;

use DB;
use Defender;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PermissionSeederTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        $permissions = config('core-media.permissions');
        
        $admin_role = config('core.admin_role');
        
        if (!$role = Defender::findRole($admin_role)) {
            $role = Defender::createRole($admin_role);
        }
        
        foreach ($permissions as $permission) {
            
            $name = $permission['name'];
            $readable_name = $permission['readable_name'];
            if (! $perm = DB::table('permissions')->where('name', '=', $name)->first()) {
               	$perm = DB::table('permissions')->insert(['name' => $name, 'readable_name' => $readable_name, 'created_at' => new DateTime, 'updated_at' => new DateTime]);


            }

           $permission = Defender::findPermission($name);

            $role->attachPermission($permission, [
                'value' => -1
            ]);
        }
    }
}
